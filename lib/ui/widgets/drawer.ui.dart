import 'package:flutter/material.dart';
import 'package:prime/ui/widgets/drawer.title.ui.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40),
            child: ListView(
              padding: EdgeInsets.only(left: 0.0, top: 16.0),
              children: [
                DrawerTitle(Icons.home, "Início", () => Navigator.of(context).pop()),
                Divider( color: Theme.of(context).primaryColorLight,),
                DrawerTitle(Icons.widgets, "Configurações", (){}),
                Divider( color: Theme.of(context).primaryColorLight,),
                DrawerTitle(Icons.exit_to_app, "Sair", (){}),
              ],
            ),
          )
        ],
      ),
    );
  }
}
