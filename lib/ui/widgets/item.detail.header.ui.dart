import 'package:flutter/material.dart';
import 'dart:math';

class ItemDetailHeader extends SliverPersistentHeaderDelegate{

  String imagePath;
  int placeId;

  ItemDetailHeader(this.imagePath, this.placeId);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
        fit: StackFit.expand,
        children: [
          GestureDetector(
            onTap: () => {
              //TODO Show tagged people
            },
            child: Hero(
              tag: "place-${Random().nextInt(10000)}",
              child: Image.asset(imagePath, fit: BoxFit.cover),
            ),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: -1,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.03,
              child: ClipRRect(
                borderRadius: BorderRadius.vertical(top: Radius.circular(40)),
                child: Container(
                  color: Colors.white,
                  child: null,
                ),
              ),
            ),
          )
        ]
    );
  }

  @override
  double get maxExtent => 490;

  @override
  double get minExtent => 0;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

}