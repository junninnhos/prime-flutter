import 'dart:math';

import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/pages/home/detail.item.page.dart';
import 'package:prime/util/item.click.util.dart';
import 'package:provider/provider.dart';

class SingleAsset extends StatelessWidget with OnItemCLick {

  int index;

  SingleAsset(this.index);

  @override
  Widget build(BuildContext context) {

    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: GestureDetector(
        onTap: onItemClick(context, index, bloc.list[index].assets.length - 1),
        child: Hero(
            tag: "place-${Random().nextInt(10000)}",
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(bloc.list[index].assets[0].path),
                        fit: BoxFit.cover
                    )
                ),
              ),
            )
        ),
      ),
    );
  }

  @override
  GestureTapCallback onItemClick(BuildContext context, int index, int imageIndex){
    return () => Navigator.pushNamed(context,
        ItemDetailPage.routeName,
        arguments: DetailArguments(index, imageIndex)
    );
  }
}
