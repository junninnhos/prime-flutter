import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/pages/home/detail.item.page.dart';
import 'package:prime/util/item.click.util.dart';
import 'package:provider/provider.dart';

class MultipleAssetsSwipe extends StatelessWidget with OnItemCLick {

  int index;

  MultipleAssetsSwipe(this.index);
  
  @override
  Widget build(BuildContext context) {

    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    Widget buildList(BuildContext context, int imageIndex){
      return Stack(
        children: [
          GestureDetector(
              onTap: onItemClick(context, index, imageIndex),
              child: Hero(
                tag: "place-${Random().nextInt(10000)}",
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Container(
                    height: 343,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(bloc.list[index].assets[imageIndex].path),
                            fit: BoxFit.cover
                        )
                    ),
                  ),
                ),
              )
          )
        ],
      );
    }
    
    return Swiper(
        itemCount: bloc.list[index].assets.length,
        scrollDirection: Axis.horizontal,
        loop: false,
        viewportFraction: 0.85,
        scale: 0.9,
        pagination: SwiperPagination(
            margin: EdgeInsets.only(top: 80),
            builder: DotSwiperPaginationBuilder(
                color: Color(0xFFA7A7A7),
                activeColor: Color(0xFF66A6FF),
                activeSize: 14
            )
        ),
        itemBuilder: buildList
    );
  }

  @override
  GestureTapCallback onItemClick(BuildContext context, int index, int imageIndex){
    return () => Navigator.pushNamed(context,
        ItemDetailPage.routeName,
        arguments: DetailArguments(index, imageIndex)
    );
  }
}