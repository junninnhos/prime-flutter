import 'package:prime/pages/home/detail.item.page.dart';
import 'package:prime/pages/home/home.page.dart';
import 'package:prime/pages/home/maps/map.page.dart';
import 'package:prime/pages/home/messages/message.item.page.dart';
import 'package:prime/pages/login/forgetPassword/forget.password.page.dart';
import 'package:prime/pages/login/login.page.dart';

final routes = {
  LoginPage.routeName: (context) => new LoginPage(),
  HomePage.routeName: (context) => new HomePage(),
  ForgetPasswordPage.routeName: (context) => new ForgetPasswordPage(),
  ItemDetailPage.routeName : (context) => new ItemDetailPage(),
  ItemMessagesPage.routeName : (context) => new ItemMessagesPage(),
  MapsPage.routeName : (context) => new MapsPage()
};