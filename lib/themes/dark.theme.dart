import 'package:flutter/material.dart';

const brightness = Brightness.dark;

const sdblue = const Color.fromARGB(255, 26, 37, 89); // Azul original da SdRedes
const sdblueLight = const Color.fromARGB(255, 6, 65, 120); // Azul original da SdRedes
const sdred = const Color.fromARGB(255, 180, 45, 54); // Vermelho original da SdRedes

const success = const Color.fromARGB(255, 16, 172, 132); // Verde de success
const successLight = const Color.fromARGB(255, 29, 209, 161); // Segundo verde de success

// abaixo cores que definem se o tema fica dark ou light
const nocturn = const Color.fromARGB(255, 34, 36, 40);  // Cinza bem escuro
const albine = const Color.fromARGB(255, 255, 255, 255); // Branco

ThemeData darkTheme() {
  return ThemeData(
      brightness: brightness,

      primaryColor: sdblue,
      primaryColorLight: sdred,

      primaryColorDark: brightness == Brightness.dark
          ? albine : nocturn,
      accentColor: brightness == Brightness.dark
          ? nocturn : albine,
      textTheme: new TextTheme(
          body1: new TextStyle(color:
          brightness != Brightness.dark
              ? Colors.black54 : Colors.white
          ),
          display1: new TextStyle(
            color: brightness == Brightness.dark
                ? albine : nocturn,
          ),
          button: new TextStyle(color: success)
      )
  );
}