import 'package:flutter/material.dart';

class ForgetPasswordPage extends StatelessWidget {

  static const routeName = '/recover';

  final _scalffoldkey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scalffoldkey,
        appBar: AppBar(
          title: Text("Recuperação de Senha"),
        ),
        body: Container(
          child: Text('ESQUECI SENHA'),
        )
    );
  }
}
