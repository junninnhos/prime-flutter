import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:provider/provider.dart';

class CloseFeedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    return Scaffold(
      body: Center(
          child: Container(
            padding: EdgeInsets.all(4.0),
            color: Colors.lime,
            height: 200,
            width: 200.0,
            child: Row(
              children: <Widget>[
                Flexible(
                  child: Text("Mais de 200 países criaram restrições à entrada de brasileiros por conta da nossa gestão da pandemia",
                      overflow: TextOverflow.visible),
                )
              ],
            ),
          )
      ),
    );
  }
}
