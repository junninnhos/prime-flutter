import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:provider/provider.dart';

class MessageArguments{
  final int index;

  MessageArguments(this.index);
}

class ItemMessagesPage extends StatelessWidget {

  static const routeName = '/comments';

  @override
  Widget build(BuildContext context) {

    final HomeBloc bloc = Provider.of<HomeBloc>(context);
    final MessageArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
          slivers: [
            SliverAppBar(
              title: Text('Comentários'),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index){

                    String userPhoto = bloc.list[args.index].comments[index].user.photo;

                    return Container(
                      color: Colors.red,
                      child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: userPhoto != null && userPhoto.isNotEmpty ?
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(userPhoto),
                                        fit: BoxFit.cover
                                    )
                                ),
                              ) : Icon(Icons.account_circle),
                            ),
                            Text(bloc.list[args.index].comments[index].comment)
                          ],
                      ),
                    );
                  },
                  childCount: bloc.list[args.index].comments.length
              ),
            )
          ],
      ),
    );
  }
}
