import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/pages/home/maps/map.page.dart';
import 'package:prime/pages/home/messages/message.item.page.dart';
import 'package:prime/ui/widgets/gradient.button.ui.dart';
import 'package:prime/ui/widgets/item.detail.header.ui.dart';
import 'package:provider/provider.dart';
import 'package:prime/util/reactions.util.dart' as CustomReactions;

class DetailArguments{
  final int index;
  final int imageIndex;
  DetailArguments(this.index, this.imageIndex);
}

class ItemDetailPage extends StatelessWidget {

  static const routeName = '/detail';

  final List<Reaction> reactions = CustomReactions.reactions;

  @override
  Widget build(BuildContext context) {
    final HomeBloc bloc = Provider.of<HomeBloc>(context);
    final DetailArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate : ItemDetailHeader(bloc.list[args.index].assets[args.imageIndex].path, args.imageIndex)
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RoundedGradientButton(
                          onTap: () => Navigator.pop(context),
                          gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [
                                Color(0xFF2AF598),
                                Color(0xFF009EFD)
                              ]
                          ),
                          icon: Icon(Icons.arrow_back, size: 25, color: Colors.white)
                        ),
                        RoundedGradientButton(
                            onTap: () {
                              Navigator.pushNamed(context,
                                  MapsPage.routeName,
                                  arguments: MapsArguments(0.0, 0.0)
                              );
                            },
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                                  Color(0xFF2AF598),
                                  Color(0xFF009EFD)
                                ]
                            ),
                            icon: Icon(Icons.location_on, size: 25, color: Colors.white)
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 7),
                  Text(bloc.list[args.index].assets[args.imageIndex].subtitle,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 22
                      ),
                      textAlign: TextAlign.center,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    height: 1,
                    color: Colors.grey[300],
                  ),
                  Container(
                    height: 40,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FlutterReactionButtonCheck(
                            boxDuration: Duration(milliseconds: 350),
                            onReactionChanged: (reaction, index, isChecked) {
                              isChecked = false;
                              print('reaction selected index: $index'); //TODO
                            },
                            reactions: reactions,
                            initialReaction: CustomReactions.defaultInitialReaction,
                            selectedReaction: CustomReactions.selectedReaction,
                          ),
                          GestureDetector(
                            onTap: () => {
                              Navigator.pushNamed(context,
                                  ItemMessagesPage.routeName,
                                  arguments:  MessageArguments(args.index)
                              )
                            },
                            child: Icon(
                                Icons.message,
                                color: Colors.black26
                            ),
                          ),
                          GestureDetector(
                            onTap: () => {
                              //TODO
                            },
                            child: Icon(
                                Icons.share,
                                color: Colors.black26
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
            ]),
          )
        ]
      ),
    );
  }
}
