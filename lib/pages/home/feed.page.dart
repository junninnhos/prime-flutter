import 'package:flutter/material.dart';
import 'package:prime/blocs/home.bloc.dart';
import 'package:prime/ui/widgets/circular.image.ui.dart';
import 'package:prime/ui/widgets/feed.item.ui.dart';
import 'package:prime/ui/widgets/menu_animation.ui.dart';
import 'package:prime/ui/widgets/slive.app.bar.delegate.dart';
import 'package:provider/provider.dart';

class FeedPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final HomeBloc bloc = Provider.of<HomeBloc>(context);

    Widget _buildSliveList(BuildContext context, int index){
      return FeedItem(index);
    }

    return Stack(
      children: [
        CustomScrollView(
          slivers: [
            SliverAppBar(
              title: Text('Home'),
            ),
            SliverPersistentHeader(
              delegate: SliverAppBarDelegate(
                  minHeight: 60,
                  maxHeight: 80,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(60)
                        )
                    ),
                    child: CircularImage('assets/images/bruno.png'), //FIXME
                  )
              ),
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate(
                  _buildSliveList,
                  childCount: bloc.list.length,
                )
            ),
          ],
        ),
        Container(
          alignment: Alignment.bottomRight,
          child: MenuAnimation(),
        ),
      ],
    );
  }
}
