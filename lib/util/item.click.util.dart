import 'package:flutter/material.dart';

abstract class OnItemCLick{
  GestureTapCallback onItemClick(BuildContext context, int index, int imageIndex);
}