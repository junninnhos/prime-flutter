import 'package:flutter/material.dart';
import 'package:flutter_reaction_button/flutter_reaction_button.dart';

enum Assets{
  LIKE, HAPPY, ANGRY, INLOVE, SURPRISED, SAD, MAD
}

extension AssetPath on Assets{
  String get name {
    switch(this){
      case Assets.LIKE : return 'assets/images/like.jpg';
      case Assets.HAPPY : return 'assets/images/happy.png';
      case Assets.ANGRY : return 'assets/images/angry.png';
      case Assets.INLOVE : return 'assets/images/in-love.png';
      case Assets.SURPRISED : return 'assets/images/surprised.png';
      case Assets.SAD : return 'assets/images/sad.png';
      case Assets.MAD : return 'assets/images/mad.png';
    }
  }
}

final defaultInitialReaction = Reaction(
  icon: _buildReactionsIcon(
      Assets.LIKE.name,
      Text(
        'Gostei',
        style: TextStyle(
          color: Colors.black,
        ),
      )
  ),
);

final selectedReaction = Reaction(
  icon: _buildReactionsIcon(
      Assets.LIKE.name,
      Text(
        'Gostei',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
  ),
);

final reactions = [
  Reaction(
    title: _buildTitle('Feliz'),
    previewIcon: _buildReactionsPreviewIcon(Assets.HAPPY.name),
    icon: _buildReactionsIcon(
      Assets.HAPPY.name,
      Text(
        'Feliz',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
  Reaction(
    title: _buildTitle('Raiva'),
    previewIcon: _buildReactionsPreviewIcon(Assets.ANGRY.name),
    icon: _buildReactionsIcon(
      Assets.ANGRY.name,
      Text(
        'Raiva',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
  Reaction(
    title: _buildTitle('Amei'),
    previewIcon: _buildReactionsPreviewIcon(Assets.INLOVE.name),
    icon: _buildReactionsIcon(
      Assets.INLOVE.name,
      Text(
        'Amei',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
  Reaction(
    title: _buildTitle('Triste'),
    previewIcon: _buildReactionsPreviewIcon(Assets.SAD.name),
    icon: _buildReactionsIcon(
      Assets.SAD.name,
      Text(
        'Triste',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
  Reaction(
    title: _buildTitle('Surpreso'),
    previewIcon: _buildReactionsPreviewIcon(Assets.SURPRISED.name),
    icon: _buildReactionsIcon(
      Assets.SURPRISED.name,
      Text(
        'Surpreso',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
  Reaction(
    title: _buildTitle('Raiva'),
    previewIcon: _buildReactionsPreviewIcon(Assets.MAD.name),
    icon: _buildReactionsIcon(
      Assets.MAD.name,
      Text(
        'Raiva',
        style: TextStyle(
          color: Colors.blue,
        ),
      )
    ),
  ),
];

Widget _buildTitle(String title) => Container(
  padding: const EdgeInsets.symmetric(horizontal: 7.5, vertical: 2.5),
  decoration: BoxDecoration(
    color: Colors.red,
    borderRadius: BorderRadius.circular(15),
  ),
  child: Text(
    title,
    style: TextStyle(
      color: Colors.black,
      fontSize: 10,
      fontWeight: FontWeight.bold,
    ),
  ),
);

Widget _buildReactionsPreviewIcon(String path) => Padding(
  padding: EdgeInsets.symmetric(horizontal: 3.5, vertical: 5),
  child: Image.asset(path, height: 40),
);


Widget _buildReactionsIcon(String path, Text text) => Container(
  color: Colors.transparent,
  child: Row(
    children: <Widget>[
      Image.asset(path, height: 20),
      SizedBox(width: 5),
      text,
    ],
  ),
);
