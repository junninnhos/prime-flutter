import 'package:flutter/widgets.dart';
import 'package:prime/models/feed.list.model.dart';
import 'package:prime/repositories/home/home.repository.dart';

class HomeBloc extends ChangeNotifier{

  final homeRepository = HomeRepository();

  List<Result> list = [];
  bool alreadyLoad = false;
  bool isLoading = false;

  getFeedList() async {

    FeedModel response = await homeRepository.getFeed();

    if(response != null && response.result.length > 0){
      list = List.from(response.result.reversed);
    }

    alreadyLoad = true;

    notifyListeners();
  }
}