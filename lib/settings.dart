class Settings{
  static const String apiUrl = 'http://oi:3000';
  static const String theme = 'Light';

  static const bool isDebug = true;
}