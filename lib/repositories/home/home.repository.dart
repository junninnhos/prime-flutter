import 'package:prime/models/feed.list.model.dart';
import 'package:prime/repositories/base.repository.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:prime/settings.dart';

class HomeRepository extends BaseRepository{

  Future<FeedModel> getFeed() async{
    if(Settings.isDebug){
      var result = await rootBundle.loadString("assets/mock/feed.list.json");
      return FeedModel.fromJson(json.decode(result));
    }

    return null;
  }
}